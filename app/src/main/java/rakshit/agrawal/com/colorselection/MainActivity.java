package rakshit.agrawal.com.colorselection;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener  {

    public static final String DEBUG_TAG = "My debug tag";
    ArrayList<String> colorList;

    public HashMap<String, String> colors = new HashMap<String, String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Fill the list of colors
        setColors();
    }

    public void setColors() {
        /*
        Create entries in dict of colors
         */
        colors.put("Blue","#0000FF");
        colors.put("Red","#FF0000");
        colors.put("Grey","#888888");
        colors.put("Pink","#F76194");
        colors.put("Green","#00FF00");
        colors.put("Purple","#880088");
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Sets the spinner options.
        Spinner sp = (Spinner) findViewById(R.id.spinner);

        // Extract color list as keySet from colors hashmap
        colorList =  new ArrayList<String>(colors.keySet());

        Log.i(DEBUG_TAG, "Color is " + colorList);
        //colorList.add("Red");
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, colorList);
        sp.setAdapter(myAdapter);

        // To set initial selection
        sp.setSelection(1);
        // Setting listener for selection calls
        sp.setOnItemSelectedListener(this);


    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        /*
                Dealing with item selection.
                Get the selected value and then find its color
                 */
        Log.i(DEBUG_TAG, "Item: " + parent.getItemAtPosition(pos));
        String key = (String) parent.getItemAtPosition(pos);

        // View to show color
        TextView cv = (TextView) findViewById(R.id.colorView);
        cv.setBackgroundColor(Color.parseColor(colors.get(key)));
        Log.i(DEBUG_TAG, "Color: " + colors.get(key));
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
        TextView cv = (TextView) findViewById(R.id.colorView);
        cv.setBackgroundColor(Color.parseColor("#FFFFFF"));
    }


}
